﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemExtensions.ClassExtensions
{
    public static class StringExtensions
    {
        public static string GetStringFromChars(IEnumerable<char> list)
        {
            StringBuilder res = new StringBuilder();

            foreach (char c in list)
                res.Append(c);

            return res.ToString();
        }

        public static string CleanLineEnds(this string str, string spaces)
        {
            int pos_start, pos_end;
            for (pos_start = 0; pos_start < str.Length && spaces.Contains(str[pos_start]); pos_start++) ;
            if (pos_start == str.Length)
                return "";

            for (pos_end = str.Length - 1; spaces.Contains(str[pos_end]); pos_end--) ;

            if (pos_end - pos_start != str.Length - 1)
                return str.Substring(pos_start, pos_end - pos_start + 1);
            else
                return str;
        }

        public static bool IsBlank(this string str, string spaces)
        {
            if (str.Length == 0)
                return true;

            for (int i = 0; i < str.Length; i++)
                if (!spaces.Contains(str[i]))
                    return false;

            return true;
        }

        public static string ReplaceWithStr(this string str, int startPos, int startLength, string s)
        {
            if (!Math.MathFunctions.InRange(startPos, 0, str.Length - 1) && (startPos != 0 || str.Length != 0))
                throw new Exception("StartPos out of line range");
            if ((startLength < 0 || startPos + startLength > str.Length) && (startLength != 0 || str.Length != 0))
                throw new Exception("StartLength out of line range");

            char[] result = new char[str.Length - startLength + s.Length];

            for (int i = 0; i < startPos; i++)
                result[i] = str[i];
            for (int i = 0; i < s.Length; i++)
                result[startPos + i] = s[i];
            for (int i = 0; i < result.Length - startPos - s.Length; i++)
                result[startPos + s.Length + i] = str[startPos + startLength + i];

            return new string(result);
        }

        public static string RemoveMany(this string str, List<int> sectorsStartPos, List<int> sectorsLength)
        {
            int count = System.Math.Min(sectorsStartPos.Count, sectorsLength.Count);

            for (int i = 0; i < count; i++)
            {
                if (!Math.MathFunctions.InRange(sectorsStartPos[i], 0, str.Length - 1))
                    throw new Exception("StartPos out of line range");
                if (sectorsLength[i] < 0 || sectorsStartPos[i] + sectorsLength[i] > str.Length)
                    throw new Exception("StartLength out of line range");
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                bool valid = true;
                for (int j = 0; j < count; j++)
                    if (Math.MathFunctions.InRange(i, sectorsStartPos[j], sectorsStartPos[j] + sectorsLength[j] - 1))
                        valid = false;

                if (valid)
                    result.Append(str[i]);
            }

            return result.ToString();
        }

        public static bool DetectStringInPos(this string str, int pos, string s, out int afterPos)
        {
            afterPos = -1;
            if (!DetectStringInPos(str, pos, s))
                return false;

            afterPos = pos + s.Length;
            return true;
        }

        public static bool DetectStringInPos(this string str, int pos, string s)
        {
            if (pos + s.Length - 1 >= str.Length)
                return false;

            for (int i = 0; i < s.Length; i++)
                if (str[pos + i] != s[i])
                    return false;

            return true;
        }
    }
}
