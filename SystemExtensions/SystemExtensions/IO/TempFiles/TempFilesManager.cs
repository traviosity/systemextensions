﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SystemExtensions.IO.TempFiles
{
    public class TempFilesManager : IDisposable
    {
        private static readonly string Extension = ".tmp";

        private readonly string Folder;

        private readonly LinkedList<TempFile> Files;

        public TempFilesManager(string folder)
        {
            Files = new LinkedList<TempFile>();
            Folder = folder;
        }

        public TempFile CreateTempFile()
        {
            TempFile newFile = new TempFile(Folder + GetValidName());
            Files.AddLast(newFile);
            return newFile;
        }

        public void DeleteFile(TempFile file)
        {
            if (!file.Disposed)
                file.Dispose();

            Files.Remove(file);
        }

        public void DeleteAllFiles()
        {
            foreach (TempFile file in Files)
                if (!file.Disposed)
                    file.Dispose();
            Files.Clear();
        }

        private string GetValidName()
        {
            if (!Directory.Exists(Folder))
                Directory.CreateDirectory(Folder);

            string[] files = Directory.GetFiles(Folder);

            int ValidIndex = 0;
            for (int i = 0; i < files.Length; i++)
            {
                if (Path.HasExtension(files[i]) && Path.GetExtension(files[i]) == Extension)
                {
                    if (int.TryParse(Path.GetFileNameWithoutExtension(files[i]), out int index))
                    {
                        if (ValidIndex < index)
                            break;

                        ValidIndex++;
                    }
                }
            }

            return ValidIndex.ToString() + Extension;
        }

        public void Dispose()
        {
            DeleteAllFiles();
        }
    }

    public class TempFile : IDisposable
    {
        public readonly string Path;

        public bool Disposed { get; private set; }

        public TempFile(string path)
        {
            File.Create(path).Close();
            Path = path;
        }

        public void Dispose()
        {
            File.Delete(Path);
            Disposed = true;
        }
    }
}
