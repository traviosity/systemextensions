﻿using System;
using System.IO;

namespace SystemExtensions.IO
{
    public class BitReader : IDisposable
    {
        private readonly BinaryReader Reader;

        private byte? ByteBuffer;
        private int BufferPos;

        public bool EndOfStream
        {
            get
            {
                return Reader.BaseStream.Position == Reader.BaseStream.Length && BufferPos == 8;
            }
        }

        public BitReader(Stream stream)
        {
            Reader = new BinaryReader(stream);

            ByteBuffer = null;
            BufferPos = 0;
        }

        public bool ReadBit()
        {
            if (BufferPos == 8 || ByteBuffer == null)
            {
                ByteBuffer = Reader.ReadByte();
                BufferPos = 0;
            }

            bool result = (ByteBuffer & (128 >> BufferPos)) != 0;
            BufferPos++;

            return result;
        }

        public void Dispose()
        {
            Reader.Dispose();
        }
    }
}
