﻿using System;
using System.IO;

namespace SystemExtensions.IO
{
    public class BitWriter : IDisposable
    {
        private readonly BinaryWriter Writer;

        private readonly bool[] ByteBuffer;
        public int BufferLength { get; private set; }

        private byte Byte
        {
            get
            {
                byte result = 0;
                for (int i = 0; i < ByteBuffer.Length; i++)
                {
                    if (ByteBuffer[i])
                        result |= (byte)(128 >> i);
                }

                return result;
            }
        }

        public BitWriter(Stream stream)
        {
            Writer = new BinaryWriter(stream);

            ByteBuffer = new bool[8];
            BufferLength = 0;
        }

        public void WriteBit(bool value)
        {
            ByteBuffer[BufferLength] = value;
            BufferLength++;

            if (BufferLength == ByteBuffer.Length)
            {
                Writer.Write(Byte);
                BufferLength = 0;
            }
        }

        public void Dispose()
        {
            Writer.Dispose();
        }
    }
}
