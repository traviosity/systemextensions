﻿namespace SystemExtensions.Math
{
    public static class MathFunctions
    {
        public static bool InRange(int value, int min, int max)
        {
            return value >= min && value <= max;
        }
    }
}
