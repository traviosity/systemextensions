﻿namespace SystemExtensions.Math
{
    public class Integrator
    {
        public int Value;

        public Integrator()
        {
            Value = 0;
        }

        public void Integrate(int value = 1)
        {
            Value += value;
        }

        public void Reset()
        {
            Value = 0;
        }
    }
}
