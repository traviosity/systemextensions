﻿using System.Collections.Generic;

namespace SystemExtensions.ObjectsManagement
{
    public class ObjectsPull<Key, Obj>
    {
        private readonly Dictionary<Key, LinkedList<Obj>> Storage;

        public ObjectsPull()
        {
            Storage = new Dictionary<Key, LinkedList<Obj>>();
        }

        public void Push(Key key, Obj obj)
        {
            if (!Storage.TryGetValue(key, out LinkedList<Obj> objCollection))
            {
                objCollection = new LinkedList<Obj>();
                Storage.Add(key, objCollection);
            }

            objCollection.AddLast(obj);
        }

        public bool ContainsObjects(Key key)
        {
            return Storage.ContainsKey(key);
        }

        public Obj this[Key key]
        {
            get
            {
                LinkedList<Obj> objCollection = Storage[key];
                Obj result = objCollection.Last.Value;
                objCollection.RemoveLast();

                if (objCollection.Count == 0)
                    Storage.Remove(key);

                return result;
            }
        }

        public bool TryPull(Key key, out Obj obj)
        {
            obj = default;

            if (!ContainsObjects(key))
                return false;

            obj = this[key];

            return true;
        }

        public void Clear()
        {
            Storage.Clear();
        }
    }
}
